package com.github.aminmsvi.yourkuya

import android.app.Application
import com.github.aminmsvi.yourkuya.di.dataAccessModule
import com.github.aminmsvi.yourkuya.di.networkModule
import com.github.aminmsvi.yourkuya.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(
                presentationModule,
                dataAccessModule,
                networkModule
            )
        }
    }
}