package com.github.aminmsvi.yourkuya.dataaccess

import com.github.aminmsvi.yourkuya.dataaccess.remote.ProductRemoteDataSource
import com.github.aminmsvi.yourkuya.model.ProductCatalog
import com.github.aminmsvi.yourkuya.model.RepoResult
import com.google.android.gms.maps.model.LatLng

interface ProductRepository {
    suspend fun getProductCatalog(latLng: LatLng): RepoResult<List<ProductCatalog>>
}

class ProductRepositoryImpl(
    private val remote: ProductRemoteDataSource
) : ProductRepository {
    override suspend fun getProductCatalog(latLng: LatLng): RepoResult<List<ProductCatalog>> {
        return remote.getProductCatalogs(latLng)
    }
}