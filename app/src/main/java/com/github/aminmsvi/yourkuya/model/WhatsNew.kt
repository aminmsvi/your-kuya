package com.github.aminmsvi.yourkuya.model

data class WhatsNew(
    val image: Int,
    val title: String,
    val description: String
)