package com.github.aminmsvi.yourkuya.di

import com.github.aminmsvi.yourkuya.BuildConfig
import com.github.aminmsvi.yourkuya.dataaccess.remote.ApiService
import com.github.aminmsvi.yourkuya.utils.calladapter.ApiCallAdapterFactory
import com.github.aminmsvi.yourkuya.utils.calladapter.ApiExceptionJsonAdapter
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = module {
    single {
        Moshi.Builder()
            .add(ApiExceptionJsonAdapter())
            .build()
    }

    single {
        OkHttpClient.Builder()
            .addInterceptor {
                val request = it.request().newBuilder()
                request.addHeader("authorization", BuildConfig.AUTHORIZATION_TOKEN)
                it.proceed(request.build())
            }
            .addInterceptor(HttpLoggingInterceptor()
                .apply {
                    setLevel(
                        if (BuildConfig.DEBUG)
                            HttpLoggingInterceptor.Level.BODY
                        else
                            HttpLoggingInterceptor.Level.NONE
                    )
                })
            .build()
    }

    single {
        Retrofit.Builder()
            .addCallAdapterFactory(
                ApiCallAdapterFactory.create(
                    get()
                )
            )
            .addConverterFactory(
                MoshiConverterFactory.create(
                    get()
                )
            )
            .client(get())
            .baseUrl(BuildConfig.API_URL)
            .build()
            .create(ApiService::class.java)
    }
}