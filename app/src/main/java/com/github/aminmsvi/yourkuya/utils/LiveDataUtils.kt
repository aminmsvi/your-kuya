package com.github.aminmsvi.yourkuya.utils

import androidx.lifecycle.MutableLiveData

fun MutableLiveData<*>.reEmit() {
    this.value = this.value
}