package com.github.aminmsvi.yourkuya.utils.calladapter

import com.squareup.moshi.Moshi
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class ApiCall<T : Any>(
    private val delegate: Call<T>,
    private val moshi: Moshi
) : Call<ApiResponse<T>> {

    override fun enqueue(callback: Callback<ApiResponse<T>>) {
        delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                val body = response.body()
                val error = response.errorBody()

                if (response.isSuccessful) {
                    if (body != null) {
                        callback.onResponse(
                            this@ApiCall,
                            Response.success(
                                ApiResponse.Success(body, response.code())
                            )
                        )
                    } else {
                        callback.onResponse(
                            this@ApiCall,
                            Response.success(
                                ApiResponse.ServerError(extractApiException(response))
                            )
                        )
                    }
                } else {
                    callback.onResponse(
                        this@ApiCall,
                        Response.success(
                            ApiResponse.ServerError(extractApiException(response))
                        )
                    )
                }
            }

            override fun onFailure(call: Call<T>, throwable: Throwable) {
                val networkResponse = when (throwable) {
                    is IOException -> ApiResponse.NetworkError(throwable)
                    is HttpException -> ApiResponse.ServerError(
                        extractApiException(throwable.response())
                    )
                    else -> throw throwable
                }
                callback.onResponse(this@ApiCall, Response.success(networkResponse))
            }
        })
    }

    override fun timeout(): Timeout = delegate.timeout()

    override fun isExecuted(): Boolean = delegate.isExecuted

    override fun clone(): Call<ApiResponse<T>> =
        ApiCall(delegate.clone(), moshi)

    override fun isCanceled(): Boolean = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<ApiResponse<T>> = throw UnsupportedOperationException()

    override fun request(): Request = delegate.request()

    private fun extractApiException(response: Response<*>?): ApiException {
        if (response == null) return ApiException.invalidResponse()
        val errorBody = response.errorBody()
        if (errorBody == null || errorBody.contentLength() == 0L)
            return ApiException.invalidResponse()

        val adapter = moshi.adapter(ApiException::class.java)
        return adapter.fromJson(errorBody.string()) ?: ApiException.invalidResponse()
    }
}