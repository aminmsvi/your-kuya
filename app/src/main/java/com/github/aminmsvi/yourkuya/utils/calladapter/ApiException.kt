package com.github.aminmsvi.yourkuya.utils.calladapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.ToJson

@JsonClass(generateAdapter = false)
data class ApiException(
    @Json(name = "code")
    val code: String = "",
    @Json(name = "message")
    override val message: String = ""
) : Exception(message) {
    companion object {
        fun invalidResponse() = ApiException(
            "INVALID_RESPONSE",
            "Retrofit received empty response"
        )
    }
}

@JsonClass(generateAdapter = true)
data class ApiExceptionJson(
    val code: String = "",
    val message: String = ""
)

class ApiExceptionJsonAdapter {
    @ToJson
    fun toJson(apiException: ApiException): ApiExceptionJson {
        return ApiExceptionJson(
            code = apiException.code,
            message = apiException.message
        )
    }

    @FromJson
    fun fromJson(json: ApiExceptionJson): ApiException {
        return ApiException(
            code = json.code,
            message = json.message
        )
    }
}