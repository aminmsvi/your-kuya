package com.github.aminmsvi.yourkuya.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Product(
    @Json(name = "id")
    val id: Int = 0,
    @Json(name = "commercialName")
    val commercialName: String = "",
    @Json(name = "icon")
    val icon: String = "",
    @Json(name = "isTwoWay")
    val isTwoWay: Boolean = false,
    @Json(name = "maxPoints")
    val maxPoints: Int = 0,
    @Json(name = "minPoints")
    val minPoints: Int = 0,
    @Json(name = "customTags")
    val customTags: List<Any> = listOf(),
    @Json(name = "categoryTags")
    val categoryTags: List<Any> = listOf(),
    @Json(name = "supportedGeoIds")
    val supportedGeoIds: List<Any> = listOf()
)