package com.github.aminmsvi.yourkuya.presentation.home

import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.model.Product
import com.github.aminmsvi.yourkuya.model.WhatsNew
import com.github.aminmsvi.yourkuya.presentation.map.MapFragment
import com.github.aminmsvi.yourkuya.utils.dp
import com.github.aminmsvi.yourkuya.widgets.GridSpacingItemDecoration
import com.github.aminmsvi.yourkuya.widgets.HorizontalItemDecoration
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment_header.*
import kotlinx.android.synthetic.main.home_fragment_products.*
import kotlinx.android.synthetic.main.home_fragment_whats_new.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(R.layout.home_fragment) {

    private val viewModel by viewModel<HomeViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(MapFragment.MAP_REQUEST_KEY) { _: String, bundle: Bundle ->
            val latLng = bundle.getParcelable<LatLng>(MapFragment.MAP_RESULT_LOCATION)
            viewModel.onLocationChanged(latLng)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_toggle_products.setOnClickListener {
            viewModel.onToggleProductsClick((rv_products.adapter as ProductAdapter).state)
        }

        tv_location.setOnClickListener {
            findNavController().navigate(
                R.id.action_homeFragment_to_mapFragment
            )
        }

        with(rv_products) {
            adapter = ProductAdapter()
            addItemDecoration(
                GridSpacingItemDecoration(
                    spanCount = 3,
                    spacing = dp(16).toInt(),
                    includeEdge = false
                )
            )
        }

        with(rv_featured_products) {
            setHasFixedSize(true)
            addItemDecoration(
                GridSpacingItemDecoration(
                    spanCount = 3,
                    spacing = dp(16).toInt(),
                    includeEdge = false
                )
            )
            adapter = ProductAdapter()
        }

        rv_what_s_new.addItemDecoration(
            HorizontalItemDecoration(
                drawable = ShapeDrawable().apply {
                    intrinsicWidth = dp(16).toInt()
                    paint.color = Color.TRANSPARENT
                }
            )
        )

        btn_retry.setOnClickListener {
            viewModel.retry()
        }

        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            container_content.isVisible = it is UiState.Data
            progress.isVisible = it is UiState.Loading
            container_error.isVisible = it is UiState.Error

            when (it) {
                is UiState.Data -> {
                    (rv_products.adapter as ProductAdapter).submitList(it.products)
                    (rv_featured_products.adapter as ProductAdapter).submitList(it.featuredProducts)
                    rv_what_s_new.adapter = WhatsNewAdapter(it.whatsNew)
                }
                is UiState.Error -> {
                }
            }
        })

        viewModel.productsListState.observe(viewLifecycleOwner, Observer {
            val adapter = (rv_products.adapter as ProductAdapter)
            when (it) {
                is ListState.Expand -> {
                    btn_toggle_products.setImageResource(R.drawable.ic_chevron_up_black_24dp)
                    adapter.setState(it)
                }
                is ListState.Collapse -> {
                    btn_toggle_products.setImageResource(R.drawable.ic_chevron_down_black_24dp)
                    adapter.setState(it)
                }
            }
        })
    }
}

sealed class ListState {
    object Expand : ListState()
    object Collapse : ListState()
}

sealed class UiState {
    data class Data(
        val featuredProducts: List<Product>,
        val products: List<Product>,
        val whatsNew: List<WhatsNew>
    ) : UiState()

    object Loading : UiState()
    object Error : UiState()
}

