package com.github.aminmsvi.yourkuya.utils.calladapter

import com.squareup.moshi.Moshi
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

class ApiCallAdapter<T : Any>(
    private val successType: Type,
    private val moshi: Moshi
) : CallAdapter<T, Call<ApiResponse<T>>> {

    override fun adapt(call: Call<T>): Call<ApiResponse<T>> =
        ApiCall(call, moshi)

    override fun responseType(): Type = successType
}