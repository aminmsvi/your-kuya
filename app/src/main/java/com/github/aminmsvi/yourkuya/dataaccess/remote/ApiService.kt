package com.github.aminmsvi.yourkuya.dataaccess.remote

import com.github.aminmsvi.yourkuya.model.ProductCatalog
import com.github.aminmsvi.yourkuya.utils.calladapter.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("product/catalog")
    suspend fun getProductCatalogs(
        @Query("lat") lat: Double,
        @Query("lng") lng: Double
    ): ApiResponse<List<ProductCatalog>>
}