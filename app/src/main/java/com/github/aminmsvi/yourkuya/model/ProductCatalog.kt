package com.github.aminmsvi.yourkuya.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductCatalog(
    @Json(name = "categoryName")
    val categoryName: String = "",
    @Json(name = "products")
    val products: List<Product> = listOf()
)
