package com.github.aminmsvi.yourkuya.presentation.map

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.utils.checkLocationPermission
import com.github.aminmsvi.yourkuya.utils.openAppSettings
import com.github.aminmsvi.yourkuya.utils.turnOnGps
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.map_fragment.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MapFragment : Fragment(R.layout.map_fragment), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    private val locationProvider by inject<LocationProvider> {
        parametersOf(viewLifecycleOwner)
    }
    private val permissionResult by lazy {
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            tryEnableMyLocation()
            moveToMyLocation()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
            .getMapAsync(this)

        btn_confirm.setOnClickListener {
            val location = googleMap?.cameraPosition?.target ?: return@setOnClickListener
            setFragmentResult(
                MAP_REQUEST_KEY,
                bundleOf(MAP_RESULT_LOCATION to LatLng(location.latitude, location.longitude))
            )
            findNavController().popBackStack()
        }

        btn_back.setOnClickListener {
            findNavController().popBackStack()
        }

        btn_my_location.setOnClickListener {
            if (context.checkLocationPermission()) {
                moveToMyLocation()
            } else {
                requestPermission()
            }
        }

        locationProvider.locationUpdateStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                is LocationUpdateStatus.GpsOff -> onGpsOff(it)
                is LocationUpdateStatus.PermissionDenied -> requestPermission()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        tryEnableMyLocation()
    }

    private fun tryEnableMyLocation() {
        if (!context.checkLocationPermission()) return
        // disable "my location" button
        googleMap?.uiSettings?.isMyLocationButtonEnabled = false
        googleMap?.isMyLocationEnabled = true
    }

    private fun moveToMyLocation() {
        if (!context.checkLocationPermission()) return
        locationProvider.lastLocation?.let {
            googleMap?.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(it.latitude, it.longitude),
                    15.0F
                )
            )
        }
    }

    private fun requestPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION))
            with(banner) {
                descriptionTextResId = R.string.map_no_permission_rationale
                iconResId = R.drawable.ic_location_disabled_24dp
                setAcceptButton(R.string.map_settings) {
                    openAppSettings()
                    dismiss()
                }
                setDeclineButton(R.string.map_dismiss) { dismiss() }
                // Wait for map to lay itself out completely
                // to prevent banner's height become messed up
                map.post { show() }
            }
        else
            with(banner) {
                descriptionTextResId = R.string.map_no_permission
                iconResId = R.drawable.ic_location_disabled_24dp
                setAcceptButton(R.string.map_grant_permission) {
                    permissionResult.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                    dismiss()
                }
                setDeclineButton(R.string.map_dismiss) { dismiss() }
                // Wait for map to lay itself out completely
                // to prevent banner's height become messed up
                map.post { show() }
            }
    }

    private fun onGpsOff(state: LocationUpdateStatus.GpsOff) {
        with(banner) {
            descriptionTextResId = R.string.map_gps_off
            iconResId = R.drawable.ic_location_disabled_24dp
            setAcceptButton(R.string.map_turn_on_gps) {
                turnOnGps(state.e)
                dismiss()
            }
            setDeclineButton(R.string.map_dismiss) { dismiss() }
            show()
        }
    }

    companion object {
        const val MAP_REQUEST_KEY = "location_picker"
        const val MAP_RESULT_LOCATION = "lat_lng"
    }
}