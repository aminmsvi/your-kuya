package com.github.aminmsvi.yourkuya.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.model.Product
import com.github.aminmsvi.yourkuya.utils.setTextAsync
import kotlinx.android.synthetic.main.product_item.view.*
import kotlin.math.abs
import kotlin.math.min

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    private val products = arrayListOf<Product>()
    var state: ListState = ListState.Collapse
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
        ProductViewHolder(parent)

    override fun getItemCount(): Int = when (state) {
        ListState.Expand -> products.size
        ListState.Collapse -> min(products.size, 6)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) =
        holder.bindTo(products[position])

    fun setState(newState: ListState) {
        val prevSize = itemCount
        state = newState
        val currentSize = itemCount
        val count = abs(prevSize - currentSize)
        if (count == 0) return
        if (prevSize > currentSize) {
            notifyItemRangeRemoved(currentSize, count)
        } else {
            notifyItemRangeInserted(prevSize, count)
        }
    }

    fun submitList(newProducts: List<Product>) {
        products.clear()
        products.addAll(newProducts)
        notifyDataSetChanged()
    }

    class ProductViewHolder(
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.product_item, parent, false)
    ) {
        fun bindTo(product: Product) {
            with(itemView) {
                img_product_icon.load(product.icon) {
                    placeholder(R.drawable.placeholder)
                }
                tv_product_name.setTextAsync(product.commercialName)
            }
        }

    }
}