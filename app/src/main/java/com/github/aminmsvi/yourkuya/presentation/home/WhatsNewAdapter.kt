package com.github.aminmsvi.yourkuya.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.model.WhatsNew
import com.github.aminmsvi.yourkuya.utils.setTextAsync
import kotlinx.android.synthetic.main.what_s_new_item.view.*

class WhatsNewAdapter(
    private val items: List<WhatsNew>
) : RecyclerView.Adapter<WhatsNewAdapter.WhatsNewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WhatsNewViewHolder =
        WhatsNewViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: WhatsNewViewHolder, position: Int) =
        holder.bindTo(items[position])

    class WhatsNewViewHolder(
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.what_s_new_item, parent, false)
    ) {
        fun bindTo(item: WhatsNew) {
            with(itemView) {
                img_image.load(item.image)
                tv_title.setTextAsync(item.title)
                tv_dsc.setTextAsync(item.description)
            }
        }
    }
}