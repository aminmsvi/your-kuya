package com.github.aminmsvi.yourkuya.presentation.home

import androidx.lifecycle.*
import com.github.aminmsvi.yourkuya.dataaccess.ProductRepository
import com.github.aminmsvi.yourkuya.dataaccess.WhatsNewRepository
import com.github.aminmsvi.yourkuya.model.Product
import com.github.aminmsvi.yourkuya.model.RepoResult
import com.github.aminmsvi.yourkuya.model.WhatsNew
import com.github.aminmsvi.yourkuya.utils.reEmit
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.async

class HomeViewModel(
    private val productRepository: ProductRepository,
    private val whatsNewRepository: WhatsNewRepository
) : ViewModel() {

    private val _geoLocation = MutableLiveData<LatLng>()
    private val _productsListState = MutableLiveData<ListState>()

    val uiState = _geoLocation.switchMap {
        liveData {
            emit(UiState.Loading)

            val whatsNewResult = viewModelScope.async { whatsNewRepository.getWhatsNew() }
            val catalogsResult = viewModelScope.async { productRepository.getProductCatalog(it) }

            var featured = emptyList<Product>()
            var products = emptyList<Product>()
            var whatsNew = emptyList<WhatsNew>()

            when (val result = catalogsResult.await()) {
                is RepoResult.Success -> {
                    products =
                        result.data.firstOrNull { it.categoryName == "All Services" }?.products
                            ?: emptyList()
                    featured =
                        result.data.firstOrNull { it.categoryName == "Featured" }?.products?.take(3)
                            ?: emptyList()
                    whatsNewResult.await()
                }
                is RepoResult.Error -> {
                    emit(UiState.Error)
                    return@liveData
                }
            }

            when (val result = whatsNewResult.await()) {
                is RepoResult.Success -> whatsNew = result.data
                is RepoResult.Error -> {
                    emit(UiState.Error)
                    return@liveData
                }
            }

            emit(UiState.Data(featured, products, whatsNew))
        }
    }

    val productsListState: LiveData<ListState>
        get() = _productsListState

    init {
        _geoLocation.value = DEFAULT_LOCATION
    }

    fun onToggleProductsClick(currentState: ListState) {
        _productsListState.value = when (currentState) {
            is ListState.Expand -> ListState.Collapse
            is ListState.Collapse -> ListState.Expand
        }
    }

    fun retry() {
        _geoLocation.reEmit()
    }

    fun onLocationChanged(latLng: LatLng?) {
        latLng?.run { _geoLocation.value = this }
    }

    companion object {
        val DEFAULT_LOCATION = LatLng(35.7232582, 51.4031547)
    }
}