package com.github.aminmsvi.yourkuya.dataaccess.remote

import com.github.aminmsvi.yourkuya.model.ProductCatalog
import com.github.aminmsvi.yourkuya.model.RepoResult
import com.github.aminmsvi.yourkuya.utils.calladapter.ApiResponse
import com.google.android.gms.maps.model.LatLng

class ProductRemoteDataSource(
    private val apiService: ApiService
) {
    suspend fun getProductCatalogs(latLng: LatLng): RepoResult<List<ProductCatalog>> =
        when (val response = apiService.getProductCatalogs(latLng.latitude, latLng.longitude)) {
            is ApiResponse.Success -> RepoResult.Success(response.data)
            is ApiResponse.ServerError -> RepoResult.Error(response.error)
            is ApiResponse.NetworkError -> RepoResult.Error(response.error)
        }
}