package com.github.aminmsvi.yourkuya.utils.calladapter

import com.squareup.moshi.Moshi
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class ApiCallAdapterFactory private constructor(
    private val moshi: Moshi
) : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        check(returnType is ParameterizedType) { "$returnType must be parameterized. Raw types are not supported" }

        val containerType = getParameterUpperBound(0, returnType)
        if (getRawType(containerType) != ApiResponse::class.java) {
            return null
        }

        check(containerType is ParameterizedType) { "$containerType must be parameterized. Raw types are not supported" }

        val successBodyType = getParameterUpperBound(0, containerType)

        return when (getRawType(returnType)) {
            Call::class.java -> ApiCallAdapter<Any>(successBodyType, moshi)
            else -> null
        }
    }

    companion object {
        fun create(moshi: Moshi): ApiCallAdapterFactory = ApiCallAdapterFactory(moshi)
    }
}