package com.github.aminmsvi.yourkuya.dataaccess

import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.model.RepoResult
import com.github.aminmsvi.yourkuya.model.WhatsNew

interface WhatsNewRepository {
    suspend fun getWhatsNew(): RepoResult<List<WhatsNew>>
}

class WhatsNewRepositoryImpl : WhatsNewRepository {
    override suspend fun getWhatsNew(): RepoResult<List<WhatsNew>> {
        val mockWhatsNew = listOf(
            WhatsNew(
                image = R.drawable.what_s_new_how_to_use_app,
                title = "How to Use the App",
                description = "Getting access to on-demand"
            ),
            WhatsNew(
                image = R.drawable.what_s_new_list_your_service_on_your_kuya,
                title = "List your services on YourKuya",
                description = "Do you offer man power?"
            )
        )
        return RepoResult.Success(mockWhatsNew)
    }
}