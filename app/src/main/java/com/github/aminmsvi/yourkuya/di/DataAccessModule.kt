package com.github.aminmsvi.yourkuya.di

import com.github.aminmsvi.yourkuya.dataaccess.ProductRepository
import com.github.aminmsvi.yourkuya.dataaccess.ProductRepositoryImpl
import com.github.aminmsvi.yourkuya.dataaccess.WhatsNewRepository
import com.github.aminmsvi.yourkuya.dataaccess.WhatsNewRepositoryImpl
import com.github.aminmsvi.yourkuya.dataaccess.remote.ProductRemoteDataSource
import org.koin.dsl.bind
import org.koin.dsl.module

val dataAccessModule = module {
    single {
        ProductRepositoryImpl(
            remote = get()
        )
    } bind ProductRepository::class

    single {
        WhatsNewRepositoryImpl()
    } bind WhatsNewRepository::class

    single {
        ProductRemoteDataSource(
            apiService = get()
        )
    }
}