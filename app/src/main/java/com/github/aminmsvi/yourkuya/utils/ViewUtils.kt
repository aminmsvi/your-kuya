package com.github.aminmsvi.yourkuya.utils

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.TypedValue
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import com.github.aminmsvi.yourkuya.BuildConfig
import com.google.android.gms.common.api.ResolvableApiException


fun AppCompatTextView.setTextAsync(text: CharSequence?) {
    if (text.isNullOrEmpty()) {
        setText(text)
    } else {
        setTextFuture(
            PrecomputedTextCompat.getTextFuture(
                text,
                TextViewCompat.getTextMetricsParams(this),
                null
            )
        )
    }
}

fun View.dp(value: Int): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        value.toFloat(),
        resources.displayMetrics
    )
}

fun Fragment.dp(value: Int): Float {
    return view?.dp(value) ?: 0F
}

fun Fragment.openAppSettings() {
    val intent = Intent().apply {
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        data = Uri.fromParts(
            "package",
            BuildConfig.APPLICATION_ID,
            null
        )
        flags = Intent.FLAG_ACTIVITY_NEW_TASK
    }
    startActivity(intent)
}

fun Fragment.turnOnGps(exception: Exception) {
    if (exception is ResolvableApiException) {
        try {
            exception.startResolutionForResult(requireActivity(), 2)
        } catch (e: IntentSender.SendIntentException) {
            try {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            } catch (e: Exception) {
            }
        }
    }
}

fun Context?.checkLocationPermission(): Boolean {
    if (this == null) return false
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    } else {
        return true
    }
}