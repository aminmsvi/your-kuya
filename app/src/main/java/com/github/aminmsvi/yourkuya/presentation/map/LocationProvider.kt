package com.github.aminmsvi.yourkuya.presentation.map

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import androidx.lifecycle.*
import com.github.aminmsvi.yourkuya.utils.checkLocationPermission
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

class LocationProvider(
    private val context: Context,
    lifecycleOwner: LifecycleOwner
) : LocationCallback(), LifecycleObserver {

    private val _locationUpdateStatus = MutableLiveData<LocationUpdateStatus>()
    private val fusedLocationClient by lazy {
        LocationServices.getFusedLocationProviderClient(context)
    }
    private val gpsProviderIntentFilter by lazy {
        IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)
    }

    var lastLocation: Location? = null
        private set

    private val gpsProviderBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == LocationManager.PROVIDERS_CHANGED_ACTION) {
                LocationServices
                    .getSettingsClient(context)
                    .checkLocationSettings(
                        LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest)
                            .build()
                    )
                    .addOnSuccessListener {
                        startLocationUpdate()
                    }
                    .addOnFailureListener {
                        stopLocationUpdate()
                        _locationUpdateStatus.value = LocationUpdateStatus.GpsOff(it)
                    }
            }
        }
    }

    private val locationRequest by lazy {
        LocationRequest().apply {
            interval = TimeUnit.SECONDS.toMillis(10)
            fastestInterval = TimeUnit.SECONDS.toMillis(5)
            maxWaitTime = TimeUnit.SECONDS.toMillis(20)
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
    }

    val locationUpdateStatus: LiveData<LocationUpdateStatus>
        get() = _locationUpdateStatus

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    override fun onLocationResult(result: LocationResult?) {
        if (result == null) return
        lastLocation = result.lastLocation
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun startLocationUpdate() {
        if (!context.checkLocationPermission()) {
            _locationUpdateStatus.value = LocationUpdateStatus.PermissionDenied
            return
        }

        context.registerReceiver(gpsProviderBroadcastReceiver, gpsProviderIntentFilter)

        LocationServices
            .getSettingsClient(context)
            .checkLocationSettings(
                LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest)
                    .build()
            )
            .addOnSuccessListener {
                fusedLocationClient
                    .requestLocationUpdates(
                        locationRequest,
                        this@LocationProvider,
                        Looper.getMainLooper()
                    )
                    .addOnSuccessListener {
                    }
                    .addOnFailureListener {
                        _locationUpdateStatus.value = LocationUpdateStatus.PermissionDenied
                    }
            }
            .addOnFailureListener {
                _locationUpdateStatus.value = LocationUpdateStatus.GpsOff(it)
            }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun stopLocationUpdate() {
        try {
            fusedLocationClient.removeLocationUpdates(this)
            context.unregisterReceiver(gpsProviderBroadcastReceiver)
        } catch (e: Exception) {
        }
    }
}

sealed class LocationUpdateStatus {
    object PermissionDenied : LocationUpdateStatus()
    data class GpsOff(val e: Exception) : LocationUpdateStatus()
}