package com.github.aminmsvi.yourkuya.di

import androidx.lifecycle.LifecycleOwner
import com.github.aminmsvi.yourkuya.presentation.home.HomeViewModel
import com.github.aminmsvi.yourkuya.presentation.map.LocationProvider
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel {
        HomeViewModel(
            productRepository = get(),
            whatsNewRepository = get()
        )
    }

    factory { (lifecycleOwner: LifecycleOwner) ->
        LocationProvider(
            context = androidContext(),
            lifecycleOwner = lifecycleOwner
        )
    }
}