package com.github.aminmsvi.yourkuya.model

sealed class RepoResult<out T : Any> {
    data class Success<T : Any>(val data: T) : RepoResult<T>()
    data class Error(val exception: Exception) : RepoResult<Nothing>()
}