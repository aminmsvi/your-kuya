package com.github.aminmsvi.yourkuya.widgets

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.aminmsvi.yourkuya.R
import com.github.aminmsvi.yourkuya.utils.dp
import kotlinx.android.synthetic.main.banner_view.view.*

class BannerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @StringRes
    var descriptionTextResId: Int = -1
        set(value) {
            field = value
            if (value <= 0) return
            tv_banner_dsc.text = context.getString(value)
        }

    @DrawableRes
    var iconResId: Int = -1
        set(value) {
            field = value
            if (value <= 0) return
            img_banner_icon.setImageResource(value)
        }

    init {
        View.inflate(context, R.layout.banner_view, this)
        setPadding(0, dp(24).toInt(), 0, 0)
        setBackgroundColor(Color.WHITE)
    }

    fun setAcceptButton(@StringRes textResId: Int, action: () -> Unit) {
        with(btn_banner_accept) {
            visibility = View.VISIBLE
            text = context.getString(textResId)
            setOnClickListener { action() }
        }
    }

    fun setDeclineButton(@StringRes textResId: Int, action: () -> Unit) {
        with(btn_banner_decline) {
            visibility = View.VISIBLE
            text = context.getString(textResId)
            setOnClickListener { action() }
        }
    }

    fun show() {
        // See https://stackoverflow.com/a/29684471/1216542
        val fromHeight = measuredHeight
        val widthSpec =
            MeasureSpec.makeMeasureSpec((parent as ViewGroup).width, MeasureSpec.EXACTLY)
        val heightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        measure(widthSpec, heightSpec)
        val toHeight: Int = measuredHeight

        ValueAnimator.ofInt(fromHeight, toHeight).apply {
            addUpdateListener {
                layoutParams = layoutParams.apply { height = it.animatedValue as Int }
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    layoutParams.height = 0
                    visibility = View.VISIBLE
                }
            })
            duration = (toHeight / context.resources.displayMetrics.density).toLong()
        }.start()
    }

    fun dismiss() {
        val fromHeight = measuredHeight

        ValueAnimator.ofInt(fromHeight, 0).apply {
            addUpdateListener {
                layoutParams = layoutParams.apply { height = it.animatedValue as Int }
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    visibility = View.GONE
                }
            })
            duration = (fromHeight / context.resources.displayMetrics.density).toLong()
        }.start()
    }
}