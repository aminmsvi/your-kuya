package com.github.aminmsvi.yourkuya.utils.calladapter

sealed class ApiResponse<out T : Any> {

    data class Success<T : Any>(
        val data: T,
        val code: Int
    ) : ApiResponse<T>()

    data class ServerError(
        val error: ApiException
    ) : ApiResponse<Nothing>()

    data class NetworkError(
        val error: Exception
    ) : ApiResponse<Nothing>()
}